//
//  AppDelegate.swift
//  Vision&CoreML
//
//  Created by Elisha Narida on 11/12/2017.
//  Copyright © 2017 Elisha Narida. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
 
        return true
    }
}
